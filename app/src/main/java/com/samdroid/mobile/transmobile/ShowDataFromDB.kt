package com.samdroid.mobile.transmobile

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ListView

class ShowDataFromDB : AppCompatActivity() {
    val dbHandler = DataBaseHelper(this, null)
    var dataList = ArrayList<HashMap<String, String>>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.showdata_main)
    }


    fun loadIntoList(){
        dataList.clear()
        val cursor = dbHandler.getAllRow()
        cursor!!.moveToFirst()

        while (!cursor.isAfterLast) {
            val map = HashMap<String, String>()
            map["id"] = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_ID))
            map["receiver"] = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_RECEIVER_PHONE))
            map["amount"] = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_RECEIVER_AMOUNT))
            map["reference"] = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_REFERENCE))
            map["ussd"] = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_USSD_MESSAGE))
            map["sms"] = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_SMS_MESSAGE))
            dataList.add(map)

            cursor.moveToNext()
        }
        findViewById<ListView>(R.id.listView).adapter = CustomAdapter(this@ShowDataFromDB, dataList)
        findViewById<ListView>(R.id.listView).setOnItemClickListener { _, _, i, _ ->
            val intent = Intent(this, DetailsActivity::class.java)
            intent.putExtra("id", dataList[+i]["id"])
            intent.putExtra("receiver", dataList[+i]["receiver"])
            intent.putExtra("amount", dataList[+i]["amount"])
            intent.putExtra("reference", dataList[+i]["reference"])
            intent.putExtra("ussd", dataList[+i]["ussd"])
            intent.putExtra("sms", dataList[+i]["sms"])
            startActivity(intent)
        }
    }

    public override fun onResume() {
        super.onResume()
        loadIntoList()
    }
}