package com.samdroid.mobile.transmobile.sendgrid;

public class SendGridException extends Exception {
  public SendGridException(Exception e) {
    super(e);
  }
}