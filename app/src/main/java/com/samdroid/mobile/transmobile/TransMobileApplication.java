package com.samdroid.mobile.transmobile;

import android.app.Application;
import android.content.Context;

import java.util.List;

/**
 * @author Mathias
 * @version 1.0.0 21/06/2020
 * @since 1.0.0
 */

public class TransMobileApplication extends Application {

	private static SessionManager sessionManager;
	private static Context context;

	@Override
	public void onCreate() {
		super.onCreate();
		sessionManager = new SessionManager(this);
		context = getApplicationContext();
	}

	public static SessionManager getSessionManager() {
		return sessionManager;
	}

	public static Context getContext() {
		// TODO Auto-generated method stub
		return context;
	}
}