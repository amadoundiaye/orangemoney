package com.samdroid.mobile.transmobile

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DataBaseHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
        SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {


    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(
                "CREATE TABLE $TABLE_NAME " +
                        "($COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT, $COLUMN_RECEIVER_PHONE TEXT, $COLUMN_RECEIVER_AMOUNT TEXT, $COLUMN_REFERENCE TEXT, $COLUMN_USSD_MESSAGE TEXT, $COLUMN_SMS_MESSAGE TEXT)"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun insertRow(receiver: String, amount: String, reference: String, ussd_message: String, sms_message: String) {
        val values = ContentValues()
        values.put(COLUMN_RECEIVER_PHONE, receiver)
        values.put(COLUMN_RECEIVER_AMOUNT, amount)
        values.put(COLUMN_REFERENCE, reference)
        values.put(COLUMN_USSD_MESSAGE, ussd_message)
        values.put(COLUMN_SMS_MESSAGE, sms_message)

        val db = this.writableDatabase
        db.insert(TABLE_NAME, null, values)
        db.close()
    }

    fun updateRow(row_id: String, receiver: String, amount: String, reference: String, ussd_message: String, sms_message: String) {
        val values = ContentValues()
        values.put(COLUMN_RECEIVER_PHONE, receiver)
        values.put(COLUMN_RECEIVER_AMOUNT, amount)
        values.put(COLUMN_REFERENCE, reference)
        values.put(COLUMN_USSD_MESSAGE, ussd_message)
        values.put(COLUMN_SMS_MESSAGE, sms_message)

        val db = this.writableDatabase
        db.update(TABLE_NAME, values, "$COLUMN_ID = ?", arrayOf(row_id))
        db.close()
    }

    fun updateRowUssd(reference: String, ussd_message: String) {
        val values = ContentValues()
        values.put(COLUMN_USSD_MESSAGE, ussd_message)

        val db = this.writableDatabase
        db.update(TABLE_NAME, values, "$COLUMN_REFERENCE = ?", arrayOf(reference))
        db.close()
    }

    fun updateRowSms(reference: String, sms_message: String) {
        val values = ContentValues()
        values.put(COLUMN_SMS_MESSAGE, sms_message)

        val db = this.writableDatabase
        db.update(TABLE_NAME, values, "$COLUMN_REFERENCE = ?", arrayOf(reference))
        db.close()
    }

    fun deleteRow(row_id: String) {
        val db = this.writableDatabase
        db.delete(TABLE_NAME, "$COLUMN_ID = ?", arrayOf(row_id))
        db.close()
    }

    fun getAllRow(): Cursor? {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM $TABLE_NAME", null)
    }

    fun getLastRow(): Cursor? {
        if(getAllRow()!!.moveToLast()){
            return getAllRow()
        }
        return null
    }

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "transmobile.db"
        const val TABLE_NAME = "TRANSACTIONS"

        const val COLUMN_ID = "ID"
        const val COLUMN_RECEIVER_PHONE = "RECEIVER"
        const val COLUMN_RECEIVER_AMOUNT = "AMOUNT"
        const val COLUMN_REFERENCE = "REFERENCE"
        const val COLUMN_USSD_MESSAGE = "USSD"
        const val COLUMN_SMS_MESSAGE = "SMS"
    }

}