package com.samdroid.mobile.transmobile

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.Toast

class DetailsActivity : AppCompatActivity() {
    private val dbHandler = DataBaseHelper(this, null)
    lateinit var receiverEditText:EditText
    lateinit var amountEditText:EditText
    lateinit var referenceEditText:EditText
    lateinit var ussdEditText:EditText
    lateinit var smsEditText:EditText
    lateinit var modifyId:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        receiverEditText = findViewById(R.id.receiver)
        amountEditText = findViewById(R.id.amount)
        referenceEditText = findViewById(R.id.reference)
        ussdEditText = findViewById(R.id.ussd)
        smsEditText = findViewById(R.id.sms)

        /* Check  if activity opened from List Item Click */
        if(intent.hasExtra("id")){
            modifyId = intent.getStringExtra("id")
            receiverEditText.setText(intent.getStringExtra("receiver"))
            amountEditText.setText(intent.getStringExtra("amount"))
            referenceEditText.setText(intent.getStringExtra("reference"))
            ussdEditText.setText(intent.getStringExtra("ussd"))
            smsEditText.setText(intent.getStringExtra("sms"))
            //findViewById<Button>(R.id.btnAdd).visibility = View.GONE
        }else{
            //findViewById<Button>(R.id.btnUpdate).visibility = View.GONE
            //findViewById<Button>(R.id.btnDelete).visibility = View.GONE
        }

    }


    fun add(v:View){
        val receiver = receiverEditText.text.toString()
        val amount = amountEditText.text.toString()
        val reference = referenceEditText.text.toString()
        val ussd = ussdEditText.text.toString()
        val sms = smsEditText.text.toString()
        dbHandler.insertRow(receiver, amount, reference, ussd, sms)
        Toast.makeText(this, "Data Addeded", Toast.LENGTH_SHORT).show()
        finish()
    }

    fun update(v:View){
        val receiver = receiverEditText.text.toString()
        val amount = amountEditText.text.toString()
        val reference = referenceEditText.text.toString()
        val ussd = ussdEditText.text.toString()
        val sms = smsEditText.text.toString()
        dbHandler.updateRow(modifyId, receiver, amount, reference, ussd, sms)
        Toast.makeText(this, "Data Updated", Toast.LENGTH_SHORT).show()
        finish()
    }

    fun delete(v:View){
        dbHandler.deleteRow(modifyId)
        Toast.makeText(this, "Data Deleted", Toast.LENGTH_SHORT).show()
        finish()
    }
}