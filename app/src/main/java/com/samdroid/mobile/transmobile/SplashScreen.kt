package com.samdroid.mobile.transmobile

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity

/**
 * @author Mathias
 * @version 1.0.0 21/06/2020
 * @since 1.0.0
 */

class SplashScreen : AppCompatActivity() {
    private val SPLASH_TIME_OUT:Long = 3000 // 3 sec
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash)

        Handler().postDelayed({
            // This method will be executed once the timer is over
            // Start your app main activity
            if(TransMobileApplication.getSessionManager().isFirstTimeAsking()){
                if (!TransMobileApplication.getSessionManager().getPhone().equals("") && !TransMobileApplication.getSessionManager().getIndicatif().equals("")) {
                    val intent = Intent(this, MainActivity::class.java)
                    this.finish()
                    startActivity(intent)
                }else{
                    val intent = Intent(this, ParamsActivity::class.java)
                    finish()
                    startActivity(intent)
                }
            }else{
                val intent = Intent(this, ParamsActivity::class.java)
                finish()
                startActivity(intent)
            }
            // close this activity
        }, SPLASH_TIME_OUT)
    }
}

