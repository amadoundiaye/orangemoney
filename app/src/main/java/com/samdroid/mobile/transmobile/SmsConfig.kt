package com.samdroid.mobile.transmobile

/**
 * @author Mathias
 * @version 1.0.0 21/06/2020
 * @since 1.0.0
 */

object SmsConfig {
    internal lateinit var beginIndex: String
    internal lateinit var endIndex: String
    internal lateinit var smsSenderNumbers: List<String>

    fun initializeSmsConfig(beginIndex: String, endIndex: String, vararg smsSenderNumbers: String) {
        this.beginIndex = beginIndex
        this.endIndex = endIndex
        this.smsSenderNumbers = smsSenderNumbers.toList()
    }
}