package com.samdroid.mobile.transmobile;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.samdroid.mobile.transmobile.sendgrid.SendGrid;
import com.samdroid.mobile.transmobile.sendgrid.SendGridException;

import java.io.IOException;
import static com.samdroid.mobile.transmobile.Constant.TRANSACTION_PAID;
import static com.samdroid.mobile.transmobile.Constant.TRANSACTION_USSD_ALERTE_FRAUDE;
import static com.samdroid.mobile.transmobile.Constant.TRANSACTION_USSD_ERROR;
import static com.samdroid.mobile.transmobile.Constant.TRANSACTION_USSD_SOLDE;
import static com.samdroid.mobile.transmobile.Constant.TRANSACTION_USSD_SUCCESS;

/**
 * @author Mathias
 * @version 1.0.0 21/06/2020
 * @since 1.0.0
 */

public class MainUtils {
    public static String MOBILE = "TRANSMOBILE_LOG";
    public static boolean DEBUG = true; // set to true just when debugging
    public static boolean DEV = false;

    public static boolean isConnected(Activity activity) {
        return isConnected(activity, false);
    }

    public static boolean isConnected(final Activity activity, boolean showToast) {
        NetworkInfo ni = ((ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();
        if (ni == null) {
            if (showToast) {
                activity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        ToastManager.makeText(activity,
                                "Veuillez vérifier votre connexion internet est activée.").show();
                    }
                });
            }
            return false;
        }

        if(ni.isConnected() && isOnline()){
            return true;
        }else {
            if (showToast) {
                activity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        ToastManager.makeText(activity,
                                "Veuillez vérifier que vous avez accès à internet.").show();
                    }
                });
            }
            return false;
        }
    }

    public static boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }
        return false;
    }

    public static void log(int priority, String message) {
        if (DEBUG) {
            Log.println(priority, MOBILE, message);
        }
    }

    public static void log(String message) {
        MainUtils.log(Log.INFO, message);
    }

    public static boolean isThereChar(String[] chaArray, String response){
        MainUtils.log("isThereChar => " + response);
        boolean bool = false;
        for(String element:chaArray)
        {
            MainUtils.log("isThereChar => " + element);
            if(response.contains(element)){
                bool = true;
            }
        }
        MainUtils.log("isThereChar => " + bool);
        return bool;
    }

    public static String ussdDecode(String ussdCode, String valideMontant, String valideNumero, String valideCode) {
        int montant = 0;
        String valideUssd = ussdCode.replace("montant", valideMontant);

        /*if(DEBUG){
            int min = 0;
            int max = 10;
            Random r = new Random();
            montant = r.nextInt(max - min + 1) + min;
            valideUssd = ussdCode.replace("montant", String.valueOf(montant));
        }*/
        valideUssd = valideUssd.replace("numero", valideNumero);
        valideUssd = valideUssd.replace("code", valideCode);
        MainUtils.log(valideUssd);
        return valideUssd;
    }

    //Depot vers 771292317reussi.Informations detaillees:Montant transaction:1.00FCFA ID de transaction:CI200421.1637.B98159Frais:0.00FCFA Commission:0.00FCFA Montant Net debite:1.00FCFA Nouveau solde:12.00FCFA.Orange Money.OFMS
    public static String parseMsgOm(String msg) {
        String newsms = msg.replace("  ", " ");
        newsms = newsms.replace(" . ", ". ");
        newsms = newsms.replace(".00FCFA", ",00FCFA");
        newsms = newsms.replace("ID de transaction:", "");
        newsms = newsms.replace("Commission:", "");
        newsms = newsms.replace(".Orange Money.OFMS", "");
        newsms = newsms.replace("Depot vers ", "");
        newsms = newsms.replace("Montant Net debite:", " ");
        newsms = newsms.replace("reussi.", " ");
        newsms = newsms.replace(" Nouveau solde:", "#");
        newsms = newsms.replace("Informations detaillees:Montant transaction:", " ");
        newsms = newsms.replace(". ", " ");
        newsms = newsms.replace(",00FCFA", ".00");
        newsms = newsms.replace("FCFA", "");
        //MainUtils.log("newsms => " + newsms);
        if(newsms.contains("#")){
            String[] tabSms = newsms.split("#");
            return tabSms[1];
        }
        return newsms;
    }

    public static String parseStatus(String message){
        MainUtils.log("isThereChar => "+ message);
        if(isThereChar(TRANSACTION_USSD_SOLDE, message)){
            MainUtils.log("solde nok --> "+ message);
            return Constant.TRANSACTION_FAILED;
        }else if (isThereChar(TRANSACTION_USSD_ERROR, message)){
            MainUtils.log("trx nok --> "+ message);
            return Constant.TRANSACTION_FAILED;
        }else if (isThereChar(TRANSACTION_USSD_SUCCESS, message)){
            MainUtils.log("trx ok --> "+ message);
            return TRANSACTION_PAID;
        }else if (isThereChar(TRANSACTION_USSD_ALERTE_FRAUDE, message)){
            return Constant.TRANSACTION_FAILED;
        }
        return Constant.TRANSACTION_PENDING;
    }

    public static boolean checkString(String[] stringArray, String message){
        boolean found = false;
        for (String element:stringArray ) {
            if ( element.equals(message)) {
                found = true;
            }
        }
        MainUtils.log(message + " <=> " + found);
       return found;
    }

    public static boolean checkContainedString(String[] stringArray, String message){
        boolean found = false;
        for (String element:stringArray ) {
            if ( message.contains(element)) {
                found = true;
            }
        }
        MainUtils.log(message + " <=> " + found);
        return found;
    }


    public static void notify(String message){
        //Alternate way of instantiating
        //SendGrid sendGrid = new SendGrid(SENDGRID_USERNAME,SENDGRID_PASSWORD);

        //Instantiate the object using your API key String
        SendGrid sendgrid = new SendGrid("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");

        SendGrid.Email email = new SendGrid.Email();
        email.addTo("madiorfaye11@gmail.com");
        email.setFrom("madiorfaye11@gmail.com");
        email.setSubject("Hello");
        email.setText(message);

        try {
            SendGrid.Response response = sendgrid.send(email);
        }
        catch (SendGridException e) {
            Log.e("sendError", "Error sending email");
        }
    }


}

