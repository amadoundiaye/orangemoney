package com.samdroid.mobile.transmobile

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import com.romellfudi.ussdlibrary.USSDController

/**
 * @author Mathias
 * @version 1.0.0 21/06/2020
 * @since 1.0.0
 */

class ParamsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.params)

        val permissions = arrayOf(
                Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_SMS,
                Manifest.permission.SEND_SMS)

        Permissions.check(this /*context*/, permissions, null /*rationale*/, null /*options*/, object : PermissionHandler() {
            override fun onGranted() {
                USSDController.verifyAccesibilityAccess(this@ParamsActivity)
            }

            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String?>?) {
                // permission denied, block the feature.
                finish()
            }
        })
        initForm()
    }

    fun initForm() {
        val validateButton = findViewById<Button>(R.id.valider)
        val url = findViewById<EditText>(R.id.url)
        val emailTo = findViewById<EditText>(R.id.emailTo)
        val emailFrom = findViewById<EditText>(R.id.emailFrom)
        val sendgridLogin = findViewById<EditText>(R.id.sendgridLogin)
        val sendgridPassword = findViewById<EditText>(R.id.sendgridPassword)
        val phone = findViewById<EditText>(R.id.phone)
        val indicatif = findViewById<EditText>(R.id.indicatif)
        val login = findViewById<EditText>(R.id.login)
        val password = findViewById<EditText>(R.id.password)
        // Setting On Click Listener
        validateButton.setOnClickListener {
            // Getting the user input
            val urlText = url.text
            val emailToText = emailTo.text
            val emailFromText = emailFrom.text
            val sendgridLoginText = sendgridLogin.text
            val sendgridPasswordText = sendgridPassword.text
            val indicatifText = indicatif.text
            val phoneText = phone.text
            val loginText = login.text
            val passwordText = password.text

            if(urlText.toString().equals("")){
                ToastManager.makeText(this, "Veuillez saisir une url!").show()
            }else if(indicatifText.toString().equals("")){
                ToastManager.makeText(this, "Veuillez saisir l'indicatif!").show()
            }else if(emailToText.toString().equals("")){
                ToastManager.makeText(this, "Veuillez saisir l'email To!").show()
            }else if(emailFromText.toString().equals("")){
                ToastManager.makeText(this, "Veuillez saisir l'email From!").show()
            }else if(sendgridLoginText.toString().equals("")){
                ToastManager.makeText(this, "Veuillez saisir le login sendgrid!").show()
            }else if(sendgridPasswordText.toString().equals("")){
                ToastManager.makeText(this, "Veuillez saisir le password sendgrid!").show()
            }else if(phoneText.toString().equals("")){
                ToastManager.makeText(this, "Veuillez saisir le numéro de téléphone!").show()
            }else if(loginText.toString().equals("")){
                ToastManager.makeText(this, "Veuillez saisir le login serveur!").show()
            }else if(passwordText.toString().equals("")){
                ToastManager.makeText(this, "Veuillez saisir le password serveur!").show()
            }else{
                TransMobileApplication.getSessionManager().setUrl(urlText.toString())
                TransMobileApplication.getSessionManager().setEmailTo(emailToText.toString())
                TransMobileApplication.getSessionManager().setEmailFrom(emailFromText.toString())
                TransMobileApplication.getSessionManager().setSendGridLogin(sendgridLoginText.toString())
                TransMobileApplication.getSessionManager().setSendGridPassword(sendgridPasswordText.toString())
                TransMobileApplication.getSessionManager().setIndicatif(indicatifText.toString())
                TransMobileApplication.getSessionManager().setPhone(phoneText.toString())
                TransMobileApplication.getSessionManager().setLogin(loginText.toString())
                TransMobileApplication.getSessionManager().setPassword(passwordText.toString())
                if (!TransMobileApplication.getSessionManager().getPhone().equals("") && !TransMobileApplication.getSessionManager().getIndicatif().equals("")) {
                    TransMobileApplication.getSessionManager().firstTimeAsking(true)
                    val intent = Intent(this, MainActivity::class.java)
                    this.finish()
                    startActivity(intent)
                }
            }
        }
    }
}