package com.samdroid.mobile.transmobile;

import android.content.Context;
import android.os.AsyncTask;

import com.samdroid.mobile.transmobile.sendgrid.SendGrid;
import com.samdroid.mobile.transmobile.sendgrid.SendGridException;


public class SendEmailASyncTask extends AsyncTask<Void, Void, Void> {

    //private Context mAppContext;
    private String mMsgResponse;

    private String mTo;
    private String mFrom;
    private String mSubject;
    private String mText;

    public SendEmailASyncTask(Context context, String mSubject,
                              String mText) {
        //this.mAppContext = context.getApplicationContext();
        this.mTo = TransMobileApplication.getSessionManager().getEmailTo();
        this.mFrom = TransMobileApplication.getSessionManager().getEmailFrom();
        this.mSubject = mSubject;
        this.mText = mText;
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            SendGrid sendgrid = new SendGrid(TransMobileApplication.getSessionManager().getSendGridLogin(), TransMobileApplication.getSessionManager().getSendGridPassword());

            SendGrid.Email email = new SendGrid.Email();

            // Get values from edit text to compose email
            // TODO: Validate edit texts
            email.addTo(mTo);
            email.setFrom(mFrom);
            email.setSubject(mSubject);
            email.setText(mText);

            // Send email, execute http request
            SendGrid.Response response = sendgrid.send(email);
            mMsgResponse = response.getMessage();

            MainUtils.log("SendAppExample => " + mMsgResponse);

        } catch (SendGridException e) {
            MainUtils.log("SendAppExample => " + e.toString());
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        //Toast.makeText(mAppContext, mMsgResponse, Toast.LENGTH_SHORT).show();
        MainUtils.log("SendAppExample => " + mMsgResponse);
    }
}