package com.samdroid.mobile.transmobile

import android.Manifest.permission
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.romellfudi.permission.PermissionService
import com.romellfudi.ussdlibrary.USSDApi
import com.romellfudi.ussdlibrary.USSDController
import com.samdroid.mobile.transmobile.Constant.*
import okhttp3.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

/**
 * @author Mathias
 * @version 1.0.0 21/06/2020
 * @since 1.0.0
 */

class MainFragment : Fragment() {

    private lateinit var dbHandler: DataBaseHelper
    private var result: TextView? = null
    private var referenceTransaction: String = ""
    private var receivedSmsMessage: String = ""
    private var receiverPhone: String = ""
    private var receivedUssdMessage: String = ""
    private var transactionAmount: String = ""
    private var map: HashMap<String, HashSet<String>>? = null
    private var ussdApi: USSDApi? = null
    private var menuActivity: MainActivity? = null
    private val client = OkHttpClient()
    private var BASE_URL: String? = ""
    private var pending: Int = 0

    operator fun JSONArray.iterator(): Iterator<JSONObject> =
            (0 until length()).asSequence().map { get(it) as JSONObject }.iterator()

    private lateinit var localBroadcastManager: LocalBroadcastManager
    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (SmsReceiver.INTENT_ACTION_SMS == intent.action) {
                val receivedSender = intent.getStringExtra(SmsReceiver.KEY_SMS_SENDER)
                val receivedMessage = intent.getStringExtra(SmsReceiver.KEY_SMS_MESSAGE)
                //var sms = receivedMessage.toString()
                //var sender = receivedSender.toString()
                result!!.append("\n-\n${receivedMessage}")
                if(receivedSender.equals("OrangeMoney")){
                    this@MainFragment.receivedSmsMessage = receivedMessage.toString()
                    MainUtils.log("receivedSmsMessage => $receivedSmsMessage")
                }
            }
        }
    }

    private val callback = object : PermissionService.Callback() {
        override fun onRefuse(RefusePermissions: ArrayList<String>) {
            Toast.makeText(context,
                    getString(R.string.refuse_permissions),
                    Toast.LENGTH_SHORT).show()
            activity!!.finish()
        }

        override fun onFinally() {
            // pass
        }
    }

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        BASE_URL = TransMobileApplication.getSessionManager().getUrl();
        MainUtils.log("BASE_URL" + BASE_URL)
        map = HashMap()
        map!!["KEY_LOGIN"] = HashSet(Arrays.asList("espere", "waiting", "loading", "esperando"))
        map!!["KEY_ERROR"] = HashSet(Arrays.asList("problema", "problem", "error", "null"))
        ussdApi = USSDController.getInstance(activity!!)
        menuActivity = activity as MainActivity?
        PermissionService(activity).request(
                arrayOf(permission.CALL_PHONE, permission.READ_PHONE_STATE),
                callback)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.content_op1, container, false)
        result = view.findViewById<View>(R.id.result) as TextView
        setHasOptionsMenu(false)
        dbHandler = DataBaseHelper(activity!!, null)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(isAccessibilitySettingsOn(activity!!)){
            login()
        }else{
            notify("ALERT STARTING APP","From Phone Number: " + TransMobileApplication.getSessionManager().getIndicatif() + TransMobileApplication.getSessionManager().getPhone() + "\n\nApplication stop continued non-compliance with the start-up procedure")

            val alertDialogBuilder = AlertDialog.Builder(activity!!)
            alertDialogBuilder.setTitle("USSD Accessibility permission")
            alertDialogBuilder
                    .setMessage("You must enable accessibility permissions for the app")
            alertDialogBuilder.setCancelable(true)
            alertDialogBuilder.setNeutralButton("ok") { _, _ ->activity!!.finish() }
            val alertDialog = alertDialogBuilder.create()
            alertDialog?.show()
        }

    }

    fun executeCode(code: String, reference: String){
        ussdApi = USSDController.getInstance(activity!!)
        ussdApi!!.callUSSDInvoke(code, map!!, object : USSDController.CallbackInvoke {
            override fun responseInvoke(message: String) {
                result!!.append("\n-\n$message")
                // first option list - select option 1
                ussdApi!!.send("1") {
                    result!!.append("\n-passed\n$it")
                    // second option list - select option 1
                    ussdApi!!.send("1") {
                        result!!.append("\nnot passed-\n$it")
                    }
                }
                receivedUssdMessage = message
                dbHandler.updateRowUssd(reference, message)
                Handler().postDelayed(
                        {
                            if(MainUtils.checkContainedString(TRANSACTION_USSD_SUCCESS, receivedSmsMessage)){
                                result!!.append("\n- parsedSMS\n "+ MainUtils.parseMsgOm(receivedSmsMessage))
                            }else{
                                result!!.append("\n- SMS\n "+ receivedSmsMessage)
                            }
                            dbHandler.updateRowSms(reference, receivedSmsMessage)
                            MainUtils.log("sendStatus: \n\nreceiverPhone => " + receiverPhone + "\n\nreferenceTransaction => " + referenceTransaction + "\n\nreceivedUssdMessage => " + receivedUssdMessage + "\n\nreceivedSmsMessage => " + receivedSmsMessage + "\n\nToken => " + TransMobileApplication.getSessionManager().getTokenType() + " " + TransMobileApplication.getSessionManager().getToken())
                            sendStatus(receiverPhone, referenceTransaction, receivedUssdMessage, receivedSmsMessage)
                        },
                        10000)
            }

            override fun over(message: String) {
                result!!.append("\n $message")
                receivedUssdMessage = message
                dbHandler.updateRowUssd(reference, message)

                Handler().postDelayed(
                        {
                            if(MainUtils.checkContainedString(TRANSACTION_USSD_SUCCESS, receivedSmsMessage)){
                                result!!.append("\n- parsedSMS\n "+ MainUtils.parseMsgOm(receivedSmsMessage))
                            }else{
                                result!!.append("\n- SMS\n "+ receivedSmsMessage)
                            }
                            dbHandler.updateRowSms(reference, receivedSmsMessage)
                            MainUtils.log("sendStatus: \n\nreceiverPhone => " + receiverPhone + "\n\nreferenceTransaction => " + referenceTransaction + "\n\nreceivedUssdMessage => " + receivedUssdMessage + "\n\nreceivedSmsMessage => " + receivedSmsMessage + "\n\nToken => " + TransMobileApplication.getSessionManager().getTokenType() + " " + TransMobileApplication.getSessionManager().getToken())
                            sendStatus(receiverPhone, referenceTransaction, receivedUssdMessage, receivedSmsMessage)
                        },
                        10000)
            }
        })
    }

    private fun login(){
        result!!.text = ""
        val postBody = "{}"
        val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), postBody)
        val request = Request.Builder()
                .url(BASE_URL + URL_LOGIN + TransMobileApplication.getSessionManager().getLogin() + "/" + TransMobileApplication.getSessionManager().getPassword())
                .post(body)
                .build()
        if (MainUtils.isConnected(activity, true)) {
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    activity!!.runOnUiThread {
                        result!!.append("\n-\nFail to call login url")
                        notify("HIGHT ALERT LOGIN FAILED","Fail to call login url: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + body.toString() + "\n\nResponse => ${e.message}")
                        retryLogin()
                    }
                }

                override fun onResponse(call: Call, response: Response){
                    val myResponse = response.body()!!.string()
                    activity!!.runOnUiThread {
                        if (myResponse == null) {
                            result!!.append("\n-\nLogin error")
                            notify("HIGHT ALERT LOGIN FAILED","Login error: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + body.toString() + "\n\nResponse => " + myResponse)
                            retryLogin()
                        }
                        try {
                            result!!.append("\n-Login Success")
                            notify("SUCCESS LOGIN " + TransMobileApplication.getSessionManager().getIndicatif() + "" + TransMobileApplication.getSessionManager().getPhone(),"Login Success")
                            val jsonObj = JSONObject(myResponse)
                            if (jsonObj.getBoolean(TAG_SUCCESS)) {
                                // parse JSON object here
                                TransMobileApplication.getSessionManager().setTokenType(jsonObj.getString(TAG_TOKEN_TYPE))
                                TransMobileApplication.getSessionManager().setToken(jsonObj.getString(TAG_TOKEN))
                                TransMobileApplication.getSessionManager().setScope(jsonObj.getString(TAG_SCOPE))
                                TransMobileApplication.getSessionManager().setLoginRetry(0)
                                //result!!.append("\n-\nParsing data for " + TransMobileApplication.getSessionManager().getLogin())
                                //result!!.append("\n-\nTokenType " + TransMobileApplication.getSessionManager().getTokenType())
                                //result!!.append("\n-\nToken " + TransMobileApplication.getSessionManager().getToken())
                                //result!!.append("\n-\nScope " + TransMobileApplication.getSessionManager().getScope())
                                getTransaction()
                            } else {
                                if (jsonObj.has(TAG_ERROR)) {
                                    result!!.append("\n-\n" + jsonObj.getString(TAG_ERROR))
                                } else {
                                    result!!.append("\n-\nError Parsing data for user " + TransMobileApplication.getSessionManager().getLogin())
                                }
                                retryLogin()
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            result!!.append("\n-\n${e.message}")
                            notify("HIGHT ALERT LOGIN FAILED","Login error: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + body.toString() + "\n\nResponse => ${e.message}")
                            retryLogin()
                        }
                    }
                }
            })
        } else {
            ToastManager.makeText(activity, resources.getString(R.string.enable_internet)).show()
            retryLogin()
        }
    }

    private fun getTransaction(){
        receivedSmsMessage = ""
        receivedUssdMessage = ""
        receiverPhone = ""
        transactionAmount = ""
        if (MainUtils.isConnected(activity!!, true)) {
            val request = Request.Builder()
                    .url(BASE_URL + URL_TRANSACTIONS + TransMobileApplication.getSessionManager().getIndicatif() + "/" + TransMobileApplication.getSessionManager().getPhone())
                    .addHeader("Authorization", TransMobileApplication.getSessionManager().getTokenType() + " " + TransMobileApplication.getSessionManager().getToken())
                    .addHeader("Content-Type", "application/json")
                    .get()
                    .build()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    activity!!.runOnUiThread {
                        result!!.append("\n-\nFail to call transaction url")
                        notify("HIGHT ALERT GET TRANSACTIONS FAILED", "Fail to call transaction url: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nResponse => " + e.message.toString())
                        retryTransaction()
                    }
                }

                override fun onResponse(call: Call, response: Response){
                    val myResponse = response.body()!!.string()
                    MainUtils.log("myResponse => " + myResponse)
                    if (myResponse == null || myResponse.equals("")) {
                        result!!.append("\n-\nTransaction error")
                        notify("HIGHT ALERT GET TRANSACTIONS FAILED", "Transaction error: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nResponse => " + myResponse)
                        retryTransaction()
                    }
                    activity!!.runOnUiThread {
                        try {
                            val jsonObj = JSONObject(myResponse)
                            if((jsonObj.has(TAG_STATUS_CODE))){
                                if (jsonObj.getInt(TAG_STATUS_CODE) === 200) {
                                    result!!.append("\n-Get Transaction Success")
                                    TransMobileApplication.getSessionManager().setTransactionRetry(0)
                                    result!!.append("\n-\nstatusCode " + jsonObj.getInt(TAG_STATUS_CODE))
                                    result!!.append("\n-\nreference " + jsonObj.getString(TAG_REFERENCE))
                                    result!!.append("\n-\nreceiverPhone " + jsonObj.getString(TAG_RECEIVER_PHONE))
                                    result!!.append("\n-\nreceiverAmount " + jsonObj.getString(TAG_RECEIVER_AMOUNT))
                                    TransMobileApplication.getSessionManager().setTransactionRetry(0)
                                    referenceTransaction = jsonObj.getString(TAG_REFERENCE)
                                    receiverPhone = jsonObj.getString(TAG_RECEIVER_PHONE)
                                    transactionAmount = jsonObj.getString(TAG_RECEIVER_AMOUNT)

                                    dbHandler.insertRow(receiverPhone, transactionAmount, referenceTransaction, receivedUssdMessage, receivedSmsMessage)
                                    executeCode(MainUtils.ussdDecode(jsonObj.getString(TAG_CODE_USSD), transactionAmount, receiverPhone, jsonObj.getString(TAG_SIM_PASS)),referenceTransaction)
                                } else {
                                    if (jsonObj.has(TAG_ERROR)) {
                                        result!!.append("\n-\n" + jsonObj.getString(TAG_ERROR))
                                    } else {
                                        result!!.append("\n-\nError Parsing data for user " + TransMobileApplication.getSessionManager().getLogin())
                                    }
                                    retryTransaction()
                                }
                            }else if (jsonObj.getInt(TAG_STATUS) === 404){
                                pending++
                                result!!.append("\n-Pas de transaction en PENDING $pending")
                                ToastManager.makeText(activity!!, "Pas de transaction en PENDING $pending").show()
                                //sendEmail("Pas de transaction en PENDING: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + request.body());
                                Handler().postDelayed(
                                        {
                                            getTransaction()
                                        },
                                        15000)
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            result!!.append("\n-\n${e.message}")
                            retryTransaction()
                        }
                    }
                }
            })
        } else {
            ToastManager.makeText(activity, resources.getString(R.string.enable_internet)).show()
            retryTransaction()
        }
    }

    private fun sendStatus(receiverPhone_: String, referenceTransaction_: String, receivedUssdMessage_: String, receivedSmsMessage_: String){
        val postBody = JSONObject()
        postBody.put("transfertStatus", getStatus(receivedUssdMessage_))
        postBody.put("phoneCode", TransMobileApplication.getSessionManager().getIndicatif())
        postBody.put("phoneNumber", TransMobileApplication.getSessionManager().getPhone())
        postBody.put("reference", referenceTransaction_)
        postBody.put("ussdMessage", receivedUssdMessage_)
        postBody.put("smsMessage", receivedSmsMessage_)
        MainUtils.log(getStatus(receivedSmsMessage) + " vs " + TRANSACTION_PAID)
        if(MainUtils.checkContainedString(TRANSACTION_USSD_SUCCESS, receivedSmsMessage)){
            postBody.put("solde", MainUtils.parseMsgOm(receivedSmsMessage_))
        }else{
            postBody.put("solde", "CANNOT GET SOLDE")
        }
        val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), postBody.toString())

        if (MainUtils.isConnected(activity, true)) {
            val request = Request.Builder()
                    .url(BASE_URL + URL_TRANSACTIONS_STATUS)
                    .addHeader("Authorization", TransMobileApplication.getSessionManager().getTokenType() + " " + TransMobileApplication.getSessionManager().getToken())
                    .addHeader("Content-Type", "application/json")
                    .post(body)
                    .build()
            MainUtils.log("request: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + body.toString())
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    activity!!.runOnUiThread {
                        result!!.append("\n-\nFail to call status url")
                        notify("HIGHT ALERT SEND STATUS FAILED","Fail to call status url: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + postBody.toString())
                        MainUtils.log("Fail to call status url: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + postBody.toString())
                        retryStatus(receiverPhone_, referenceTransaction_, receivedUssdMessage_, receivedSmsMessage_)
                    }
                }

                override fun onResponse(call: Call, response: Response){
                    val myResponse = response.body()!!.string()
                    if (myResponse == null) {
                        result!!.append("\n-\nStatus error")
                        notify("HIGHT ALERT SEND STATUS ERROR","Status error: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + postBody.toString() + "\n\nResponse => " + myResponse)
                        MainUtils.log("Fail to call status url: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + postBody.toString())
                        retryStatus(receiverPhone_, referenceTransaction_, receivedUssdMessage_, receivedSmsMessage_)
                    }
                    activity!!.runOnUiThread {
                        try {
                            val jsonObj = JSONObject(myResponse)
                            MainUtils.log("SendStatus => $myResponse")

                            if((jsonObj.has(TAG_SUCCESS))){
                                if (jsonObj.getBoolean(TAG_SUCCESS)) {
                                    result!!.append("\n-SendStatus success")
                                    TransMobileApplication.getSessionManager().setStatusRetry(0)
                                    ToastManager.makeText(activity, jsonObj.getString(TAG_MESSAGE)).show()
                                    notify("SEND STATUS SUCCESS","Transaction Success: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + postBody.toString() + "\n\nResponse => " + myResponse)
                                }else{
                                    result!!.append("\n-SendStatus error")
                                    notify("HIGHT ALERT SEND STATUS ERROR","Status error: \n\nRequest Url => " + request.url() + "\n\nRequest Headers => " + request.headers() + "\n\nRequest Body => " + postBody.toString() + "\n\nResponse => " + myResponse)
                                }
                            }
                            Handler().postDelayed(
                                    {
                                        getTransaction()
                                    },
                                    30000)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                            result!!.append("\n-\n${e.message}")
                            retryStatus(receiverPhone_, referenceTransaction_, receivedUssdMessage_, receivedSmsMessage_)
                        }
                    }
                }
            })
        } else {
            ToastManager.makeText(activity, resources.getString(R.string.enable_internet)).show()
            retryStatus(receiverPhone_, referenceTransaction_, receivedUssdMessage_, receivedSmsMessage_)
        }
    }

    fun retryLogin() {
        if(TransMobileApplication.getSessionManager().getLoginRetry() >= retry){
            notify("HIGHT ALERT RETRY " + TransMobileApplication.getSessionManager().getLoginRetry()+ "x LOGIN","Retry " + TransMobileApplication.getSessionManager().getTransactionRetry() +" from Phone Number " + TransMobileApplication.getSessionManager().getIndicatif() + TransMobileApplication.getSessionManager().getPhone())
            TransMobileApplication.getSessionManager().setLoginRetry(0)
            //activity!!.finishAffinity()
        }
        val i = TransMobileApplication.getSessionManager().getLoginRetry() + 1
        TransMobileApplication.getSessionManager().setLoginRetry(i)
        notify("HIGHT ALERT RETRY LOGIN","Retry " + i +" from Phone Number " + TransMobileApplication.getSessionManager().getIndicatif() + TransMobileApplication.getSessionManager().getPhone())
        login()
    }

    fun retryTransaction() {
        if(TransMobileApplication.getSessionManager().getTransactionRetry() >= retry){
            notify("HIGHT ALERT RETRY " + TransMobileApplication.getSessionManager().getTransactionRetry()+ "x GET TRANSACTION","Retry " + TransMobileApplication.getSessionManager().getTransactionRetry() +" from Phone Number " + TransMobileApplication.getSessionManager().getIndicatif() + TransMobileApplication.getSessionManager().getPhone())
            TransMobileApplication.getSessionManager().setTransactionRetry(0)
            //activity!!.finishAffinity()
        }
        val i = TransMobileApplication.getSessionManager().getTransactionRetry() + 1
        TransMobileApplication.getSessionManager().setTransactionRetry(i)
        notify("HIGHT ALERT RETRY GET TRANSACTION","Retry " + i +" from Phone Number " + TransMobileApplication.getSessionManager().getIndicatif() + TransMobileApplication.getSessionManager().getPhone())
        getTransaction()
    }

    fun retryStatus(receiverPhone_: String, referenceTransaction_: String, receivedUssdMessage_: String, receivedSmsMessage_: String) {
        val postBody = JSONObject()
        postBody.put("transfertStatus", getStatus(receivedUssdMessage_))
        postBody.put("phoneCode", TransMobileApplication.getSessionManager().getIndicatif())
        postBody.put("phoneNumber", receiverPhone_)
        postBody.put("reference", referenceTransaction_)
        postBody.put("ussdMessage", receivedUssdMessage_)
        postBody.put("smsMessage", receivedSmsMessage_)

        if(TransMobileApplication.getSessionManager().getStatusRetry() >= retry){
            notify("HIGHT ALERT MANY FAILED RETRY " + TransMobileApplication.getSessionManager().getStatusRetry()+ "x TO SEND STATUS","Failed Retry from Phone Number " + TransMobileApplication.getSessionManager().getIndicatif() + TransMobileApplication.getSessionManager().getPhone() + "\n Request Body => " + postBody.toString())
            TransMobileApplication.getSessionManager().setStatusRetry(0)
        }
        val i = TransMobileApplication.getSessionManager().getStatusRetry() + 1
        TransMobileApplication.getSessionManager().setStatusRetry(i)
        notify("HIGHT ALERT RETRY SEND STATUS", "Retry " + i +" from Phone Number " + TransMobileApplication.getSessionManager().getIndicatif() + TransMobileApplication.getSessionManager().getPhone() + "\n Request Body => " + postBody.toString())
        sendStatus(receiverPhone_, referenceTransaction_, receivedUssdMessage_, receivedSmsMessage_)
    }

    private fun notify(subject: String, message: String){
        val task = SendEmailASyncTask(activity!!,
                "" + subject,
                "Hello,\nPlease take a look at this message.\n$message")
        task.execute()
    }

    private fun getStatus(receivedUssdMessage_: String): String {
        var transfertStatus: String
        /*if (MainUtils.checkString(TRANSACTION_USSD_SOLDE, receivedUssdMessage_)) {
            transfertStatus = TRANSACTION_FAILED
        } else if (MainUtils.checkString(TRANSACTION_USSD_ERROR, receivedUssdMessage_)) {
            transfertStatus = TRANSACTION_FAILED
        } else if (MainUtils.checkString(TRANSACTION_USSD_SUCCESS, receivedUssdMessage_)) {
            transfertStatus = TRANSACTION_PAID
        } else if (MainUtils.checkString(TRANSACTION_USSD_ALERTE_FRAUDE, receivedUssdMessage_)) {
            transfertStatus = TRANSACTION_FAILED
            notify("HIGHT ALERT IDENTICAL TRANSACTION", "Identical Transaction from Phone Number " + TransMobileApplication.getSessionManager().getIndicatif() + TransMobileApplication.getSessionManager().getPhone() + " Detected.\n Close USSD Windows and Restart app manually required")
        }*/
        if (MainUtils.checkString(TRANSACTION_USSD_SUCCESS, receivedUssdMessage_)) {
            transfertStatus = TRANSACTION_PAID
        }else{
            transfertStatus = TRANSACTION_FAILED
        }
        return transfertStatus
    }

    private fun isAccessibilitySettingsOn(mContext: Context): Boolean {
        var accessibilityEnabled = 0
        val service: String = activity!!.packageName.toString() + "/" + USSDController::class.java.canonicalName
        MainUtils.log(service)
        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.applicationContext.contentResolver,
                    Settings.Secure.ACCESSIBILITY_ENABLED)
            MainUtils.log("accessibilityEnabled = $accessibilityEnabled")
        } catch (e: Settings.SettingNotFoundException) {
            MainUtils.log("Error finding setting, default accessibility to not found: "
                    + e.message)
        }
        val mStringColonSplitter = TextUtils.SimpleStringSplitter(':')
        if (accessibilityEnabled == 1) {
            MainUtils.log("***ACCESSIBILITY IS ENABLED*** -----------------")
            val settingValue = Settings.Secure.getString(
                    mContext.applicationContext.contentResolver,
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES)
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue)
                while (mStringColonSplitter.hasNext()) {
                    val accessibilityService = mStringColonSplitter.next()
                    MainUtils.log("-------------- > accessibilityService :: $accessibilityService $service")
                    if (accessibilityService.equals(service, ignoreCase = true)) {
                        MainUtils.log("We've found the correct setting - accessibility is switched on!")
                        return true
                    }
                }
            }
        } else {
            MainUtils.log("***ACCESSIBILITY IS DISABLED***")
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        callback.handler(permissions, grantResults)
    }

    private fun registerReceiver() {
        localBroadcastManager = LocalBroadcastManager.getInstance(activity!!)
        val intentFilter = IntentFilter()
        intentFilter.addAction(SmsReceiver.INTENT_ACTION_SMS)
        localBroadcastManager.registerReceiver(broadcastReceiver, intentFilter)
    }

    private fun unRegisterReceiver() {
        localBroadcastManager.unregisterReceiver(broadcastReceiver)
    }

    override fun onPause() {
        unRegisterReceiver()
        super.onPause()
    }

    override fun onResume() {
        registerReceiver()
        super.onResume()
    }


}