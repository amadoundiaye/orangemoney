package com.samdroid.mobile.transmobile

import android.content.Context
import android.content.SharedPreferences

/**
 * @author Mathias
 * @version 1.0.0 21/06/2020
 * @since 1.0.0
 */

class SessionManager(val context: Context) {
    private val PREFS_NAME = "transMobilePrefs"
    private val KEY_URL = "urlKey"
    private val KEY_EMAIL_TO = "emailToKey"
    private val KEY_EMAIL_FROM = "emailFromKey"
    private val KEY_SENDGRID_LOGIN = "sendgridLoginKey"
    private val KEY_SENDGRID_PASSWORD = "sendgridPasswordKey"
    private val KEY_TOKEN = "keyToken"
    private val KEY_TOKEN_TYPE = "keyTokenType"
    private val KEY_SCOPE = "keyScope"
    private val KEY_PHONE = "phoneKey"
    private val KEY_INDICATIF = "indicatifKey"
    private val KEY_FIRSTIME = "firstTime"
    private val KEY_LOGIN = "loginKey"
    private val KEY_PASSWORD = "passwordKey"
    private val KEY_RETRY_LOGIN = "loginRetryKey"
    private val KEY_RETRY_TRANSACTION = "transactionRetryKey"
    private val KEY_RETRY_STATUS = "statusRetryKey"

    val sharedPref: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun save(KEY_NAME: String, text: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putString(KEY_NAME, text)
        editor.commit()
    }

    fun save(KEY_NAME: String, value: Int) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putInt(KEY_NAME, value)
        editor.commit()
    }

    fun save(KEY_NAME: String, status: Boolean) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putBoolean(KEY_NAME, status)
        editor.commit()
    }

    fun getValueString(KEY_NAME: String): String? {
        return sharedPref.getString(KEY_NAME, null)
    }

    fun getValueInt(KEY_NAME: String): Int {
        return sharedPref.getInt(KEY_NAME, 0)
    }

    fun getValueBoolien(KEY_NAME: String, defaultValue: Boolean): Boolean {
        return sharedPref.getBoolean(KEY_NAME, defaultValue)
    }

    fun clearSharedPreference() {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        //sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        editor.clear()
        editor.commit()
    }

    fun removeValue(KEY_NAME: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.remove(KEY_NAME)
        editor.commit()
    }

    fun setToken(token: String) {
        save(KEY_TOKEN, token)
    }

    fun getToken(): String? {
        return sharedPref.getString(KEY_TOKEN, "")
    }

    fun setTokenType(tokenType: String) {
        save(KEY_TOKEN_TYPE, tokenType)
    }

    fun getTokenType(): String? {
        return sharedPref.getString(KEY_TOKEN_TYPE, "")
    }

    fun setScope(scope: String) {
        save(KEY_SCOPE, scope)
    }

    fun getScope(): String? {
        return sharedPref.getString(KEY_SCOPE, "")
    }

    fun setPhone(phone: String) {
        save(KEY_PHONE, phone)
    }

    fun getPhone(): String? {
        return sharedPref.getString(KEY_PHONE, "")
    }

    fun setIndicatif(indicatif: String) {
        save(KEY_INDICATIF, indicatif)
    }

    fun getIndicatif(): String? {
        return sharedPref.getString(KEY_INDICATIF, "")
    }

    fun firstTimeAsking(isFirstTime: Boolean) {
        save(KEY_FIRSTIME, isFirstTime)
    }

    fun isFirstTimeAsking(): Boolean {
        return sharedPref.getBoolean(KEY_FIRSTIME, false)
    }

    fun setLogin(login: String) {
        save(KEY_LOGIN, login)
    }

    fun getLogin(): String? {
        return sharedPref.getString(KEY_LOGIN, "")
    }

    fun setPassword(password: String) {
        save(KEY_PASSWORD, password)
    }

    fun getPassword(): String? {
        return sharedPref.getString(KEY_PASSWORD, "")
    }

    fun setLoginRetry(retryLogin: Int) {
        save(KEY_RETRY_LOGIN, retryLogin)
    }

    fun getLoginRetry(): Int {
        return sharedPref.getInt(KEY_RETRY_LOGIN, 0)
    }

    fun setTransactionRetry(retryLogin: Int) {
        save(KEY_RETRY_TRANSACTION, retryLogin)
    }

    fun getTransactionRetry(): Int {
        return sharedPref.getInt(KEY_RETRY_TRANSACTION, 0)
    }

    fun setStatusRetry(retryLogin: Int) {
        save(KEY_RETRY_STATUS, retryLogin)
    }

    fun getStatusRetry(): Int {
        return sharedPref.getInt(KEY_RETRY_STATUS, 0)
    }

    fun setUrl(url: String) {
        save(KEY_URL, url)
    }

    fun getUrl(): String? {
        return sharedPref.getString(KEY_URL, "")
    }

    fun setEmailTo(emailTo: String) {
        save(KEY_EMAIL_TO, emailTo)
    }

    fun getEmailTo(): String? {
        return sharedPref.getString(KEY_EMAIL_TO, "")
    }

    fun setEmailFrom(emailFrom: String) {
        save(KEY_EMAIL_FROM, emailFrom)
    }

    fun getEmailFrom(): String? {
        return sharedPref.getString(KEY_EMAIL_FROM, "")
    }

    fun setSendGridLogin(login: String) {
        save(KEY_SENDGRID_LOGIN, login)
    }

    fun getSendGridLogin(): String? {
        return sharedPref.getString(KEY_SENDGRID_LOGIN, "")
    }

    fun setSendGridPassword(password: String) {
        save(KEY_SENDGRID_PASSWORD, password)
    }

    fun getSendGridPassword(): String? {
        return sharedPref.getString(KEY_SENDGRID_PASSWORD, "")
    }



}
