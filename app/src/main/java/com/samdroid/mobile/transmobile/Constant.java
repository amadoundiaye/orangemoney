package com.samdroid.mobile.transmobile;

/**
 * @author Mathias
 * @version 1.0.0 21/06/2020
 * @since 1.0.0
 */

public class Constant {
    // base urls
    public static final String URL_LOGIN =  "Connect/Token/";
    public static final String URL_TRANSACTIONS =  "OrangeMoney/Transaction/";//{simCode}/{simNumber}
    public static final String URL_TRANSACTIONS_STATUS = "OrangeMoney/TransactionStatus";

    public static final Integer retry = 3 ;

    public static final String TAG_SIM_PASS = "ussdPassword";
    public static final String TAG_CODE_USSD = "ussdCode";
    public static final String TAG_SUCCESS = "success";
    public static final String TAG_TOKEN = "access_token";
    public static final String TAG_TOKEN_TYPE = "token_type";
    public static final String TAG_SCOPE = "scope";
    public static final String TAG_ERROR = "error";
    public static final String TAG_REFERENCE = "reference";
    public static final String TAG_STATUS_CODE = "statusCode";
    public static final String TAG_STATUS = "status";
    public static final String TAG_RECEIVER_PHONE = "receiverPhone";
    public static final String TAG_RECEIVER_AMOUNT = "receiverAmount";
    public static final String TAG_MESSAGE = "message";

    public static final String TRANSACTION_PENDING = "PENDING"; //En attente d’être traite
    public static final String TRANSACTION_PAID = "PAID"; //Passée avec succès
    public static final String TRANSACTION_FAILED = "FAILED"; //Une erreur est arrivée lors du traitement
    public static final String TRANSACTION_CANCELLED = "CANCELED"; //Annulée
    public static final String TRANSACTION_VALIDATED = "VALIDATED"; //En cours de traitement par le serveur

    public static final String[] TRANSACTION_USSD_SUCCESS = {"La transaction est en cours. Merci de patienter.", "confirmation", "Transaction effectué.", "confirmation par SMS.", "Depot vers", "Montant Net debite:", "reussi.Informations detaillees:Montant transaction:"};

    public static final String[] TRANSACTION_USSD_ERROR = {"erreur", "MMI", "Montant incorrect! Veuillez tapez un montant entre 0 et 1500001 FCFA", "Impossible de traiter la demande.", "initiateur", "Le destinataire n'est pas un client Orange Money", "Problème de connexion ou code IHM non valide.", "Le solde de votre compte ne vous permet pas", "incorrect", "9 chiffres", "Echec de la transaction", "le montant saisi est superieur au montant maximum autorise pour ce service", "superieur au montant maximum", "montant maximum", "plafond maximum", "Transaction a echoue avec TXN Id:", "Echec de la transaction"};

    public static final String[] TRANSACTION_USSD_SOLDE = {"Le solde de votre compte ne vous permet pas", "Vous allez recevoir un sms indiquant votre nouveau solde. Merci de patienter"};

    public static final String[] TRANSACTION_USSD_ALERTE_FRAUDE = {"Vous venez d'effectuer une transaction identique vers le meme numéro pour le meme montant", "Vous venez d'effectuer une transaction identique vers le meme numero pour le meme montant", "transaction identique"};

}