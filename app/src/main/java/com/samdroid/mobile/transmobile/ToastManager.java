package com.samdroid.mobile.transmobile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

/**
 * @author Mathias
 * @version 1.0.0 21/06/2020
 * @since 1.0.0
 */

public class ToastManager {

    public static Toast makeText(Context context, int text) {
        return makeText(context, context.getResources().getString(text));
    }

    @SuppressLint("InflateParams")
    public static Toast makeText(Context context, String text) {
        return Toast.makeText(context, text, Toast.LENGTH_LONG);
    }
}